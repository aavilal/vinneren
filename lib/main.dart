import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/shared/theme.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/pages/splash.dart';
import 'package:vinneren/pages/auth.dart';
import 'package:vinneren/pages/home.dart';
import 'package:vinneren/models/enum-authmode.dart';
import 'package:vinneren/models/enum-appmode.dart';
import 'package:vinneren/shared/mycolors.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();
  AuthMode _authMode = AuthMode.Initial;

  startTime() {
    return new Timer(Duration(milliseconds: 3500), _model.autoAuthenticate);
  }

  @override
  void initState() {
    startTime();
    _model.authModeSubject.listen((AuthMode authMode) {
      setState(() {
        _authMode = authMode;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: Colors.transparent,
        systemNavigationBarColor: MyColors.LighterGrey,
        systemNavigationBarIconBrightness: Brightness.dark));

    return ScopedModel<MainModel>(
      model: _model,
      child: MaterialApp(
        title: 'Vinneren',
        theme: getThemeData(context),
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (BuildContext context) => _authMode == AuthMode.Initial
              ? SplashPage()
              : _authMode == AuthMode.NotAuthenticated
              ? AuthPage()
              : HomePage(model: _model, appMode: AppMode.Marketplace,),
        },
        onGenerateRoute: (RouteSettings settings) {
          if (_authMode == AuthMode.NotAuthenticated) {
            return MaterialPageRoute<bool>(
              builder: (BuildContext context) => AuthPage(),
            );
          }
          final List<String> pathElements = settings.name.split('/');
          if (pathElements[0] != '') {
            return null;
          }
          /*
          if (pathElements[1] == 'product') {
            final String productId = pathElements[2];
            final Product product =
            _model.allProducts.firstWhere((Product product) {
              return product.id == productId;
            });
            return CustomRoute<bool>(
              builder: (BuildContext context) =>
              !_isAuthenticated ? AuthPage() : ProductPage(product),
            );
          }
          */
          return null;
        },
        onUnknownRoute: (RouteSettings settings) {
          return MaterialPageRoute(
              builder: (BuildContext context) => _authMode == AuthMode.Initial
                  ? SplashPage()
                  : _authMode == AuthMode.NotAuthenticated
                  ? AuthPage()
                  : HomePage(model: _model, appMode: AppMode.Marketplace));
        },
      ),
    );
  }
}
