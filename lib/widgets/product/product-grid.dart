import 'package:flutter/material.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/widgets/product/product-item.dart';
import 'package:vinneren/widgets/product/product-item-shimmer.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:vinneren/widgets/loader/adapative_progress_indicator.dart';
import 'package:vinneren/pages/mp-product-list-filters.dart';
import 'package:vinneren/shared/theme.dart';
import 'package:vinneren/widgets/sort/sort.dart';
import 'package:vinneren/models/enum-plp-view.dart';

class ProductGridWidget extends StatefulWidget {
  final String keyWidget;
  ProductGridWidget({@required this.keyWidget});

  @override
  State<StatefulWidget> createState() {
    return _ProductGridWidgetState();
  }
}

class _ProductGridWidgetState extends State<ProductGridWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return StickyHeader(
          header: Container(
              height: 40.0,
              decoration: BoxDecoration(color: MyColors.White, boxShadow: [
                BoxShadow(
                    color: MyColors.Grey.withOpacity(0.15),
                    blurRadius: 4.0,
                    spreadRadius: 1.0,
                    offset: Offset(0.0, 2.0))
              ]),
              child: Center(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    child: Text(
                      'SORT',
                      style: ThemeText.tsText6,
                    ),
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return SortWidget(
                              sortMode: model.sort,
                            );
                          });
                    },
                  ),
                  FlatButton(
                    child: Text(
                      'FILTER',
                      style: ThemeText.tsText6,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(PageRouteBuilder(
                        pageBuilder: (_, __, ___) => MPProductListFiltersPage(
                              model: model,
                            ),
                        transitionsBuilder: (_, Animation<double> animation, __,
                            Widget widget) {
                          return Opacity(
                            opacity: animation.value,
                            child: widget,
                          );
                        },
                        transitionDuration: Duration(milliseconds: 500),
                      ));
                    },
                  ),
                ],
              ))),
          content: model.products == null
              ? Container(
                  width: double.infinity,
                  padding: EdgeInsets.only(top: 56.0, right: 16.0, left: 16.0),
                  child: Center(
                    child: Text(
                      'No products were found matching your selection',
                      style: ThemeText.tsText9,
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              : Stack(
                  children: <Widget>[
                    StaggeredGridView.countBuilder(
                        shrinkWrap: true,
                        padding: EdgeInsets.symmetric(
                            horizontal: 8.0, vertical: 12.0),
                        primary: false,
                        crossAxisSpacing: 12.0,
                        mainAxisSpacing: 12.0,
                        crossAxisCount: model.viewMode == PLPViewMode.Grid ? 2 : 1,
                        staggeredTileBuilder: (int index) =>
                            StaggeredTile.fit(1),
                        itemCount: model.products.length > 0
                            ? model.products.length
                            : 10,
                        itemBuilder: (BuildContext context, int index) =>
                            model.products.length > 0
                                ? ProductItemWidget(
                                    product: model.products[index],
                                    viewMode: model.viewMode,
                                    keyWidget: DateTime.now()
                                            .millisecondsSinceEpoch
                                            .toString() +
                                        widget.keyWidget.toString(),
                                  )
                                : ProductItemShimmerWidget()),
                    Positioned(
                      child: model.isLoading
                          ? AdaptiveProgressIndicator()
                          : Container(),
                      bottom: 10.0,
                      left: deviceWidth * 0.5 - 20.0,
                    )
                  ],
                ));
    });
  }
}
