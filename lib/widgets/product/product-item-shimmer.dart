import 'package:flutter/material.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:shimmer/shimmer.dart';

class ProductItemShimmerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    return InkWell(
      highlightColor: MyColors.DarkBlue.withOpacity(0.4),
      splashColor: MyColors.DarkBlue.withOpacity(0.4),
      borderRadius: BorderRadius.all(Radius.circular(5)),
      child: Container(
          width: (deviceWidth / 2.0) - 12.0,
          padding: EdgeInsets.only(bottom: 8.0),
          decoration: BoxDecoration(
              color: MyColors.White,
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              boxShadow: [
                BoxShadow(
                    color: MyColors.Grey.withOpacity(0.15),
                    blurRadius: 4.0,
                    spreadRadius: 1.0,
                    offset: Offset(4.0, 1.0))
              ]),
          child: Wrap(children: <Widget>[
            Shimmer.fromColors(
              baseColor: MyColors.LightGrey,
              highlightColor: MyColors.LighterGrey,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      height: 150.0,
                      decoration: BoxDecoration(
                        color: MyColors.Black,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5.0),
                            topRight: Radius.circular(5.0)),
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 14.0,
                        color: MyColors.Black,
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 14.0,
                        width: deviceWidth / 4,
                        color: MyColors.Black,
                      ),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 14.0,
                        width: deviceWidth / 4,
                        color: MyColors.Black,
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 14.0,
                        color: MyColors.Black,
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 14.0,
                        color: MyColors.Black,
                      ),
                    ),
                  ]),
            )
          ])),
      onTap: () {},
    );
  }
}
