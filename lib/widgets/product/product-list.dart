import 'package:flutter/material.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/models/product.dart';
import 'package:vinneren/widgets/product/product-item.dart';
import 'package:vinneren/widgets/product/product-item-shimmer.dart';
import 'package:vinneren/shared/theme.dart';
import 'package:vinneren/models/enum-plp-view.dart';
import 'package:vinneren/models/enum-plpmode.dart';
import 'package:vinneren/pages/mp-product-list.dart';

class ProductListWidget extends StatefulWidget {
  final MainModel model;
  final String keyWidget;
  final dynamic data;
  ProductListWidget(
      {@required this.model, @required this.keyWidget, @required this.data});

  @override
  State<StatefulWidget> createState() {
    return _ProductListWidgetState();
  }
}

class _ProductListWidgetState extends State<ProductListWidget> {
  List<Product> _products = new List();
  @override
  void initState() {
    getData();
    super.initState();
  }

  void getData() async {
    Map<String, dynamic> response = await widget.model.setSearch(
        '', '?fq=productClusterIds:' + widget.data['clusterId'].toString());
    setState(() {
      _products = response['data'];
    });
  }

  List<Widget> _buildProductList() {
    List<Widget> list = new List();
    int index = 0;
    list.add(SizedBox(
      width: 8,
    ));
    for (final dynamic element in _products) {
      list.add(ProductItemWidget(
        product: element,
        viewMode: PLPViewMode.Grid,
        keyWidget: widget.keyWidget,
      ));
      list.add(SizedBox(
        width: 12,
      ));
      index++;
      if (index >= widget.data['maxItems']) {
        break;
      }
    }
    list.removeLast();
    list.add(SizedBox(
      width: 8,
    ));
    return list;
  }

  List<Widget> _buildProductShimmerList() {
    List<Widget> list = new List();
    list.add(SizedBox(
      width: 8,
    ));
    for (var i = 0; i < widget.data['maxItems']; i++) {
      list.add(ProductItemShimmerWidget());
      list.add(SizedBox(
        width: 12,
      ));
    }
    list.removeLast();
    list.add(SizedBox(
      width: 8,
    ));
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 370,
      margin: EdgeInsets.only(bottom: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              bottom: 8,
              left: 16.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(widget.data['title'],
                    style: Theme.of(context).textTheme.title),
                FlatButton(
                  child: Text(
                    'See more',
                    style: ThemeText.tsText6,
                    textAlign: TextAlign.right,
                  ),
                  onPressed: () {
                    dynamic detail = widget.data['detail'];
                    detail['clusterId'] = widget.data['clusterId'];
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => MPProductListPage(
                        type: PLPMode.Cluster,
                        title: widget.data['title'],
                        data: detail,
                        model: widget.model,
                      ),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));

                  },
                )
              ],
            ),
          ),
          Expanded(
            child: ListView(
              padding: EdgeInsets.only(bottom: 8),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: _products.isEmpty
                  ? _buildProductShimmerList()
                  : _buildProductList(),
            ),
          ),
        ],
      ),
    );
  }
}
