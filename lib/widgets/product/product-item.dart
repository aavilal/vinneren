import 'package:flutter/material.dart';
import 'package:vinneren/models/product.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:vinneren/shared/theme.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:vinneren/models/enum-plp-view.dart';

class ProductItemWidget extends StatelessWidget {
  final Product product;
  final PLPViewMode viewMode;
  final String keyWidget;
  ProductItemWidget(
      {@required this.product,
      @required this.viewMode,
      @required this.keyWidget});

  double _getStockSize(int stock, BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double factor = viewMode == PLPViewMode.Grid ? 0.5 : 1.0;
    final double stockMaxSize = deviceWidth * factor - 16.0;
    final int stockMaxAmount = 30;
    stock = stock > stockMaxAmount ? stockMaxAmount : stock;
    return stock * stockMaxSize / stockMaxAmount;
  }

  Color _getStockColor(int stock) {
    final int midAmount = 20;
    final int minAmount = 9;
    return stock <= minAmount
        ? MyColors.Red
        : stock <= midAmount ? MyColors.Orange : MyColors.Green;
  }

  Widget _buildViewGrid(BuildContext context, double deviceWidth) {
    return Container(
      width: (deviceWidth / 2.0) - 12.0,
      height: 300.0,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
              bottom: 0.0,
              child: Container(
                decoration: BoxDecoration(
                    color: MyColors.White,
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    boxShadow: [
                      BoxShadow(
                          color: MyColors.Grey.withOpacity(0.15),
                          blurRadius: 4.0,
                          spreadRadius: 1.0,
                          offset: Offset(4.0, 1.0))
                    ]),
                child: Wrap(
                  children: <Widget>[
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Hero(
                              tag: 'hero-grid-$keyWidget-${product.id}',
                              child: Material(
                                child: Container(
                                  child: FadeInImage(
                                    placeholder: AssetImage(
                                        'assets/images/not-available.png'),
                                    image: NetworkImage(product.image),
                                    height: 150.0,
                                    width: double.infinity,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            height: 30.0,
                            child: Text(
                              product.title,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: ThemeText.tsText1,
                            ),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              FlutterMoneyFormatter(
                                          amount: product.discountPrice * 1.0)
                                      .output
                                      .symbolOnLeft +
                                  ' MXN',
                              style: ThemeText.tsText3,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              product.discountPrice == product.price
                                  ? ''
                                  : FlutterMoneyFormatter(
                                              amount: product.price * 1.0)
                                          .output
                                          .symbolOnLeft +
                                      ' MXN',
                              style: ThemeText.tsText2,
                            ),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              'Sold by ' + product.seller,
                              style: ThemeText.tsText4,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  (product.stock > 100
                                          ? '100+'
                                          : product.stock.toString()) +
                                      ' available',
                                  style: ThemeText.tsText5,
                                ),
                                Container(
                                  height: 5.0,
                                  width: _getStockSize(product.stock, context),
                                  decoration: BoxDecoration(
                                      color: _getStockColor(product.stock),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(4.0)),
                                      shape: BoxShape.rectangle),
                                ),
                              ],
                            ),
                          ),
                        ])
                  ],
                ),
              )),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                highlightColor: MyColors.DarkBlue.withOpacity(0.4),
                splashColor: MyColors.DarkBlue.withOpacity(0.4),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildViewLarge(BuildContext context, double deviceWidth) {
    return Container(
      width: deviceWidth - 12.0,
      height: 470.0,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
              bottom: 0.0,
              child: Container(
                decoration: BoxDecoration(
                    color: MyColors.White,
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    boxShadow: [
                      BoxShadow(
                          color: MyColors.Grey.withOpacity(0.15),
                          blurRadius: 4.0,
                          spreadRadius: 1.0,
                          offset: Offset(4.0, 1.0))
                    ]),
                child: Wrap(
                  children: <Widget>[
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Hero(
                              tag: 'hero-grid-$keyWidget-${product.id}',
                              child: Material(
                                child: Container(
                                  child: FadeInImage(
                                    placeholder: AssetImage(
                                        'assets/images/not-available.png'),
                                    image: NetworkImage(product.image),
                                    height: 300.0,
                                    width: double.infinity,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 24.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            height: 30.0,
                            child: Text(
                              product.title,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: ThemeText.tsText1,
                            ),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              FlutterMoneyFormatter(
                                  amount: product.discountPrice * 1.0)
                                  .output
                                  .symbolOnLeft +
                                  ' MXN',
                              style: ThemeText.tsText13,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              product.discountPrice == product.price
                                  ? ''
                                  : FlutterMoneyFormatter(
                                  amount: product.price * 1.0)
                                  .output
                                  .symbolOnLeft +
                                  ' MXN',
                              style: ThemeText.tsText2,
                            ),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              'Sold by ' + product.seller,
                              style: ThemeText.tsText4,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  (product.stock > 100
                                      ? '100+'
                                      : product.stock.toString()) +
                                      ' available',
                                  style: ThemeText.tsText5,
                                ),
                                Container(
                                  height: 5.0,
                                  width: _getStockSize(product.stock, context),
                                  decoration: BoxDecoration(
                                      color: _getStockColor(product.stock),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(4.0)),
                                      shape: BoxShape.rectangle),
                                ),
                              ],
                            ),
                          ),
                        ])
                  ],
                ),
              )),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                highlightColor: MyColors.DarkBlue.withOpacity(0.4),
                splashColor: MyColors.DarkBlue.withOpacity(0.4),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildViewList(BuildContext context, double deviceWidth) {
    return Container(
      width: deviceWidth - 12.0,
      height: 150.0,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
              bottom: 0.0,
              child: Container(
                decoration: BoxDecoration(
                    color: MyColors.White,
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    boxShadow: [
                      BoxShadow(
                          color: MyColors.Grey.withOpacity(0.15),
                          blurRadius: 4.0,
                          spreadRadius: 1.0,
                          offset: Offset(4.0, 1.0))
                    ]),
                child: Wrap(
                  children: <Widget>[
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Hero(
                              tag: 'hero-grid-$keyWidget-${product.id}',
                              child: Material(
                                child: Container(
                                  child: FadeInImage(
                                    placeholder: AssetImage(
                                        'assets/images/not-available.png'),
                                    image: NetworkImage(product.image),
                                    height: 150.0,
                                    width: 150.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )),
                          Container(
                            width: deviceWidth - 150.0 - 16.0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(
                                  height: 12.0,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                  height: 30.0,
                                  child: Text(
                                    product.title,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: ThemeText.tsText1,
                                  ),
                                ),
                                SizedBox(
                                  height: 8.0,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Text(
                                    FlutterMoneyFormatter(
                                        amount: product.discountPrice * 1.0)
                                        .output
                                        .symbolOnLeft +
                                        ' MXN',
                                    style: ThemeText.tsText3,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Text(
                                    product.discountPrice == product.price
                                        ? ''
                                        : FlutterMoneyFormatter(
                                        amount: product.price * 1.0)
                                        .output
                                        .symbolOnLeft +
                                        ' MXN',
                                    style: ThemeText.tsText2,
                                  ),
                                ),
                                SizedBox(
                                  height: 8.0,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Text(
                                    'Sold by ' + product.seller,
                                    style: ThemeText.tsText4,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                                SizedBox(
                                  height: 8.0,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        (product.stock > 100
                                            ? '100+'
                                            : product.stock.toString()) +
                                            ' available',
                                        style: ThemeText.tsText5,
                                      ),
                                      Container(
                                        height: 5.0,
                                        width: _getStockSize(product.stock, context),
                                        decoration: BoxDecoration(
                                            color: _getStockColor(product.stock),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0)),
                                            shape: BoxShape.rectangle),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ])
                  ],
                ),
              )),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                highlightColor: MyColors.DarkBlue.withOpacity(0.4),
                splashColor: MyColors.DarkBlue.withOpacity(0.4),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    return viewMode == PLPViewMode.Grid
        ? _buildViewGrid(context, deviceWidth)
        : viewMode == PLPViewMode.Large
            ? _buildViewLarge(context, deviceWidth)
            : _buildViewList(context, deviceWidth);
  }
}
