import 'package:flutter/material.dart';
import 'package:vinneren/widgets/carousel/carousel.dart';
import 'package:vinneren/models/enum-plpmode.dart';
import 'package:vinneren/pages/mp-product-list.dart';
import 'package:vinneren/scoped-models/main.dart';

class BannerWidget extends StatelessWidget {
  final MainModel model;
  final List<dynamic> data;
  BannerWidget({@required this.model, @required this.data});

  List<NetworkImage> _buildImages() {
    List<NetworkImage> list = new List();
    data.forEach((dynamic element) {
      list.add(NetworkImage(element['image'].toString()));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      height: 250.0,
      margin: EdgeInsets.only(bottom: 24),
      child: Carousel(
        boxFit: BoxFit.cover,
        dotColor: Colors.white,
        dotSize: 5.5,
        dotSpacing: 16.0,
        dotBgColor: Colors.transparent,
        showIndicator: true,
        overlayShadow: true,
        overlayShadowColors: Colors.white.withOpacity(0.9),
        overlayShadowSize: 0.1,
        animationDuration: Duration(seconds: 1),
        images: _buildImages(),
        onImageTap: (index) {

          Navigator.of(context).push(PageRouteBuilder(
            pageBuilder: (_, __, ___) => MPProductListPage(
              type: PLPMode.Cluster,
              title: data[index]['detail']['title'],
              data: data[index]['detail'],
              model: model,
            ),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget widget) {
              return Opacity(
                opacity: animation.value,
                child: widget,
              );
            },
            transitionDuration: Duration(milliseconds: 500),
          ));
        },
      ),
    );
  }
}
