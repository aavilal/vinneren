import 'package:flutter/material.dart';

class StaticBannerWidget extends StatelessWidget {
  final String urlImage;
  StaticBannerWidget({@required this.urlImage});


  @override
  Widget build(BuildContext context) {

    return Container(
      height: 150.0,
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.only(bottom: 12.0),
      child: FadeInImage(
        placeholder: AssetImage(
            'assets/images/not-available.png'),
        image: NetworkImage(urlImage),
        width: double.infinity,
        fit: BoxFit.cover,
      ),
    );
  }
}
