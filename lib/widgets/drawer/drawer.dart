import 'package:flutter/material.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/models/enum-option.dart';
import 'package:vinneren/models/enum-appmode.dart';
import 'package:vinneren/widgets/wave/wave.dart';
import 'package:vinneren/widgets/wave/config.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:vinneren/pages/profile.dart';
import 'package:vinneren/pages/home.dart';
import 'package:vinneren/pages/mp-categories.dart';
import 'package:vinneren/pages/mp-orders.dart';
import 'package:vinneren/pages/help.dart';
import 'package:vinneren/widgets/alert/alert.dart';
import 'package:vinneren/shared/theme.dart';

class DrawerWidget {
  //final MainModel model;
  final Option option;
  DrawerWidget({@required this.option});

  Widget _buildBackground() {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Container(
        child: WaveWidget(
          config: CustomConfig(
            colors: [
              Colors.white70,
              Colors.white54,
              Colors.white30,
              Colors.white,
            ],
            durations: [32000, 21000, 18000, 5000],
            heightPercentages: [0.85, 0.86, 0.88, 0.91],
          ),
          backgroundColor: MyColors.DarkBlue,
          size: Size(double.infinity, double.infinity),
          waveAmplitude: 0,
        ),
      ),
    );
  }

  String _formatText(String text) {
    String formatText = '';
    List<String> words = text.toLowerCase().split(' ');
    words.forEach((String word) {
      formatText = formatText +
          ' ' +
          word.substring(0, 1).toUpperCase() +
          word.substring(1).toLowerCase();
    });
    return formatText.trim();
  }

  Widget _buildHeaderText(BuildContext context, MainModel model) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 24, right: 8, top: 24),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: MyColors.White,
                  foregroundColor: MyColors.DarkBlue,
                  child: Text(
                    model.user != null
                        ? model.user.firstName != null
                            ? model.user.firstName.substring(0, 1)
                            : model.user.email.substring(0, 1)
                        : '',
                    style: ThemeText.tsText8,
                  ),
                ),
                SizedBox(
                  width: 12.0,
                ),
                Expanded(
                  child: Text(
                    model.user != null
                        ? model.user.firstName != null &&
                        model.user.firstName.length > 0
                        ? _formatText(
                        model.user.firstName + ' ' + model.user.lastName)
                        : model.user.email
                        : '',
                    style: Theme.of(context).textTheme.display1,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                )

              ],
            ),
            SizedBox(
              height: 8.0,
            ),
            FlatButton(
              padding: EdgeInsets.all(0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'View Profile',
                    style: Theme.of(context).textTheme.display2,
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: Colors.white,
                  )
                ],
              ),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(PageRouteBuilder(
                  pageBuilder: (_, __, ___) => ProfilePage(),
                  transitionsBuilder:
                      (_, Animation<double> animation, __, Widget widget) {
                    return Opacity(
                      opacity: animation.value,
                      child: widget,
                    );
                  },
                  transitionDuration: Duration(milliseconds: 500),
                ));
              },
            )
          ]),
    );
  }

  getDrawer(BuildContext context) {
    return Drawer(
      child: ScopedModelDescendant<MainModel>(
          builder: (BuildContext context, Widget child, MainModel model) {
        return ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
                decoration: BoxDecoration(color: MyColors.DarkBlue),
                padding: EdgeInsets.zero,
                child: Stack(children: <Widget>[
                  _buildBackground(),
                  _buildHeaderText(context, model),
                ])),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: option == Option.Marketplace
                    ? MyColors.DarkBlue.withOpacity(0.2)
                    : Colors.transparent,
              ),
              child: Container(
                child: ListTile(
                  selected: option == Option.Marketplace,
                  leading: Icon(Icons.home),
                  title: Text('Marketplace'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => HomePage(
                            model: model,
                            appMode: AppMode.Marketplace,
                          ),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: option == Option.Categories
                    ? MyColors.DarkBlue.withOpacity(0.2)
                    : Colors.transparent,
              ),
              child: Container(
                padding: EdgeInsets.only(left: 54),
                child: ListTile(
                  selected: option == Option.Categories,
                  leading: Icon(Icons.widgets),
                  title: Text('Categories'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => MPCategoriesPage(),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: option == Option.Orders
                    ? MyColors.DarkBlue.withOpacity(0.2)
                    : Colors.transparent,
              ),
              child: Container(
                padding: EdgeInsets.only(left: 54),
                child: ListTile(
                  selected: option == Option.Orders,
                  leading: Icon(Icons.receipt),
                  title: Text('Orders'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => MPOrdersPage(
                            model: model,
                          ),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: option == Option.Wallet
                    ? MyColors.DarkBlue.withOpacity(0.2)
                    : Colors.transparent,
              ),
              child: Container(
                child: ListTile(
                  selected: option == Option.Wallet,
                  leading: Icon(Icons.payment),
                  title: Text('Wallet'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => new HomePage(
                            model: model,
                            appMode: AppMode.Wallet,
                          ),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 8, right: 8, bottom: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: option == Option.PaymentServices
                    ? MyColors.DarkBlue.withOpacity(0.2)
                    : Colors.transparent,
              ),
              child: Container(
                child: ListTile(
                  selected: option == Option.PaymentServices,
                  leading: Icon(Icons.apps),
                  title: Text('Services'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => HomePage(
                            model: model,
                            appMode: AppMode.PaymentServices,
                          ),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));
                  },
                ),
              ),
            ),
            Divider(
              color: MyColors.GreyBrown,
              height: 1,
            ),
            Container(
              margin: EdgeInsets.only(left: 8, right: 8, top: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: option == Option.AboutVinneren
                    ? MyColors.DarkBlue.withOpacity(0.2)
                    : Colors.transparent,
              ),
              child: Container(
                child: ListTile(
                  selected: option == Option.AboutVinneren,
                  leading: Icon(Icons.smartphone),
                  title: Text('About Vinneren'),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertWidget(
                            title: 'Vinneren',
                            messages: [
                              'App for Marketplace, Wallet & Payment of Services...',
                              'Created by Vinneren.',
                              'OrderFormId: ' + model.orderForm.orderFormId
                            ],
                          );
                        });
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 8, right: 8, top: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: option == Option.Help
                    ? MyColors.DarkBlue.withOpacity(0.2)
                    : Colors.transparent,
              ),
              child: Container(
                child: ListTile(
                  selected: option == Option.Help,
                  leading: Icon(Icons.help),
                  title: Text('Help'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => HelpPage(
                            model: model,
                          ),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 8, right: 8, top: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: Colors.transparent,
              ),
              child: Container(
                child: ListTile(
                  trailing: Icon(Icons.input),
                  selected: false,
                  title: Text(
                    'Log out',
                    textAlign: TextAlign.right,
                  ),
                  onTap: () {
                    model.logout();
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        '/auth', (Route r) => r == null);
                    //Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
                  },
                ),
              ),
            ),
          ],
        );
      }),
    );
  }
}
