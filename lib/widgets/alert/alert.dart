import 'package:flutter/material.dart';

class AlertWidget extends StatelessWidget {
  final String title;
  final List<String> messages;

  AlertWidget({@required this.title, @required this.messages});

  List<Widget> getMessage(BuildContext context, List<String> messages) {
    List<Widget> list = List<Widget>();
    messages.forEach((String message) {
      list.add(Container(
        margin: EdgeInsets.only(bottom: 16),
        child: Text(
          message,
          style: Theme.of(context).inputDecorationTheme.labelStyle,
        ),
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        this.title,
        style: Theme.of(context).textTheme.subhead,
      ),
      content: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: getMessage(context, this.messages),
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('CLOSE'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
