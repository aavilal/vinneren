import 'package:flutter/material.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:vinneren/models/enum-plpmode.dart';
import 'package:vinneren/pages/mp-product-list.dart';
import 'package:vinneren/scoped-models/main.dart';

class BoxBannerWidget extends StatelessWidget {
  final MainModel model;
  final dynamic data;
  BoxBannerWidget({@required this.model, @required this.data});

  List<Widget> _buildBoxBanner(BuildContext context, double deviceWidth) {
    List<Widget> list = new List();
    data['data'].forEach((dynamic element) {
      list.add(SizedBox(
        width: 8,
      ));
      list.add(
        Ink.image(
          image: NetworkImage(element['image']),
          fit: BoxFit.cover,
          child: InkWell(
            highlightColor: MyColors.DarkBlue.withOpacity(0.4),
            splashColor: MyColors.DarkBlue.withOpacity(0.4),
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: Container(
              width: deviceWidth * 0.32,
            ),
            onTap: () {
              Navigator.of(context).push(PageRouteBuilder(
                pageBuilder: (_, __, ___) => MPProductListPage(
                  type: PLPMode.Cluster,
                  title: element['detail']['title'],
                  data: element['detail'],
                  model: model,
                ),
                transitionsBuilder:
                    (_, Animation<double> animation, __, Widget widget) {
                  return Opacity(
                    opacity: animation.value,
                    child: widget,
                  );
                },
                transitionDuration: Duration(milliseconds: 500),
              ));
            },
          ),
        ),
      );
    });
    list.add(SizedBox(
      width: 8,
    ));
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    return Container(
      width: deviceWidth,
      height: deviceWidth * 0.3 + 36,
      margin: EdgeInsets.only(bottom: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              bottom: 8,
              left: 16.0,
            ),
            child:
                Text(data['title'], style: Theme.of(context).textTheme.title),
          ),
          Expanded(
            child: ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: _buildBoxBanner(context, deviceWidth),
            ),
          ),
        ],
      ),
    );
  }
}
