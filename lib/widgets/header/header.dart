import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/models/enum-appmode.dart';
import 'package:vinneren/pages/mp-search.dart';
import 'package:vinneren/pages/mp-cart.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/shared/mycolors.dart';

class HeaderWidget {
  final AppMode appMode;
  final String title;
  List<Widget> actionButtons;
  double _opacity = 0.0;
  HeaderWidget({@required this.appMode, @required this.title});

  getAppBar(BuildContext context) {
    actionButtons = new List<Widget>();
    if (this.appMode == AppMode.Marketplace) {
      _opacity = 1.0;
    } else {
      _opacity = 0.0;
    }
    actionButtons.add(AnimatedOpacity(
      opacity: _opacity,
      duration: Duration(seconds: 1),
      child: ScopedModelDescendant<MainModel>(
          builder: (BuildContext context, Widget child, MainModel model) {
        return IconButton(
            icon: Icon(Icons.search),
            color: Colors.white,
            onPressed: () {
              if (this.appMode == AppMode.Marketplace)
                Navigator.of(context).push(PageRouteBuilder(
                  pageBuilder: (_, __, ___) => MPSearchPage(
                        model: model,
                      ),
                  transitionsBuilder:
                      (_, Animation<double> animation, __, Widget widget) {
                    return Opacity(
                      opacity: animation.value,
                      child: widget,
                    );
                  },
                  transitionDuration: Duration(milliseconds: 500),
                ));
            });
      }),
    ));

    actionButtons.add(AnimatedOpacity(
      opacity: _opacity,
      duration: Duration(seconds: 1),
      child: Stack(
        children: <Widget>[
          ScopedModelDescendant<MainModel>(
              builder: (BuildContext context, Widget child, MainModel model) {
            return IconButton(
                icon: Icon(Icons.shopping_cart),
                color: Colors.white,
                onPressed: () {
                  if (this.appMode == AppMode.Marketplace)
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => MPCartPage(
                            model: model,
                          ),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));
                });
          }),
          Positioned(
            top: 0,
            right: 4,
            child: Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                color: MyColors.LightBlue,
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              child: Center(
                child: Text(
                  '2',
                  style: Theme.of(context).textTheme.overline,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          )
        ],
      ),
    ));

    return AppBar(
        title: Text(this.title),
        toolbarOpacity: 1,
        elevation: 2.0,
        actions: actionButtons);
  }
}
