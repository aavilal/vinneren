import 'package:flutter/material.dart';
import 'package:vinneren/shared/theme.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'dart:async';
import 'package:vinneren/models/filter.dart';
import 'package:vinneren/models/filter-option.dart';

// ignore: must_be_immutable
class FilterWidget extends StatefulWidget {
  Filter filter;
  FilterWidget({@required this.filter});

  @override
  State<StatefulWidget> createState() {
    return _FilterWidgetState();
  }
}

class _FilterWidgetState extends State<FilterWidget> {
  FilterOption _filterSelected;
  bool _block;
  @override
  void initState() {
    _filterSelected = widget.filter.filterSelected;
    _block = false;
    super.initState();

  }

  void _handleOnChange(FilterOption filterOption, MainModel model) {
    if (!_block) {
      setState(() {
        _filterSelected = filterOption;
      });
      model.setFilterSelected(widget.filter, filterOption);
      _block = true;
    }
    Timer(Duration(milliseconds: 500), () => Navigator.of(context).pop());
  }

  List<Widget> _buildOptions(MainModel model) {
    List<Widget> list = List<Widget>();
    list.add(InkWell(
      child: Row(
        children: <Widget>[
          Radio(
              value: null,
              groupValue: _filterSelected,
              onChanged: (sortMode) => _handleOnChange(null, model)),
          Expanded(
            child: Text(
              'ALL',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: ThemeText.tsText10,
            ),
          )
        ],
      ),
      onTap: () => _handleOnChange(null, model),
    ));
    widget.filter.filterOptions.forEach((FilterOption filterOption) {
      list.add(InkWell(
        child: Row(
          children: <Widget>[
            Radio(
                value: filterOption,
                groupValue: _filterSelected,
                onChanged: (sortMode) => _handleOnChange(filterOption, model)),
            Expanded(
              child: Text(
                filterOption.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: ThemeText.tsText10,
              ),
            )
          ],
        ),
        onTap: () => _handleOnChange(filterOption, model),
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return AlertDialog(
        title: Text(
          widget.filter.name,
          style: Theme.of(context).textTheme.subhead,
        ),
        content: Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _buildOptions(model),
            ),
          ),
        ),
      );
    });
  }
}
