import 'package:flutter/material.dart';
import 'package:vinneren/widgets/category/category-item.dart';
import 'package:vinneren/models/taxonomy.dart';
import 'package:vinneren/shared/theme.dart';
import 'package:vinneren/pages/mp-categories.dart';
import 'package:vinneren/scoped-models/main.dart';

class CategoryListWidget extends StatelessWidget {
  final MainModel model;
  final Map<String, dynamic> data;
  CategoryListWidget({@required this.model, @required this.data});

  List<Widget> _buildCategory(BuildContext context, double deviceWidth) {
    List<Widget> list = new List();
    List<Widget> listColumnElement = new List();
    Taxonomy taxonomyFound;

    int index = 0;
    data['data'].forEach((dynamic element) {
      taxonomyFound = model.taxonomy.firstWhere(
          (elementTaxonomy) => elementTaxonomy.id == element['categoryId'],
          orElse: () => null);
      dynamic detail = element['detail'];
      detail['categoryId'] = element['categoryId'];
      if (taxonomyFound != null) {
        if (index.isEven) {
          list.add(SizedBox(
            width: 8,
          ));
          listColumnElement = new List();
          listColumnElement.add(CategoryItemWidget(
              model: model, text: taxonomyFound.name, urlImage: element['image'], detail: detail));
          listColumnElement.add(SizedBox(
            height: 8,
          ));
        } else {
          listColumnElement.add(CategoryItemWidget(
              model: model, text: taxonomyFound.name, urlImage: element['image'], detail: detail));
          list.add(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: listColumnElement));
        }
      }
      index = index + 1;
    });
    list.add(SizedBox(
      width: 8,
    ));
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;

    return Container(
      width: deviceWidth,
      height: deviceWidth * (0.25 * 2) + 36 + 28,
      margin: EdgeInsets.only(bottom: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              bottom: 8,
              left: 16.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(data['title'], style: Theme.of(context).textTheme.title),
                FlatButton(
                  child: Text(
                    'See all',
                    style: ThemeText.tsText6,
                    textAlign: TextAlign.right,
                  ),
                  onPressed: () {
                    Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => MPCategoriesPage(),
                      transitionsBuilder:
                          (_, Animation<double> animation, __, Widget widget) {
                        return Opacity(
                          opacity: animation.value,
                          child: widget,
                        );
                      },
                      transitionDuration: Duration(milliseconds: 500),
                    ));
                  },
                )
              ],
            ),
          ),
          Expanded(
            child: ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: _buildCategory(context, deviceWidth),
            ),
          ),
        ],
      ),
    );
  }
}
