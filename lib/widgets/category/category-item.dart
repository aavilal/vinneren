import 'package:flutter/material.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:vinneren/pages/mp-product-list.dart';
import 'package:vinneren/models/enum-plpmode.dart';
import 'package:vinneren/scoped-models/main.dart';

class CategoryItemWidget extends StatelessWidget {
  final MainModel model;
  final String text;
  final String urlImage;
  final dynamic detail;
  CategoryItemWidget({this.model, @required this.text, @required this.urlImage, this.detail});

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;

    return Ink.image(
        image: NetworkImage(urlImage),
        fit: BoxFit.cover,
        child: InkWell(
          highlightColor: MyColors.DarkBlue.withOpacity(0.4),
          splashColor: MyColors.DarkBlue.withOpacity(0.4),
          borderRadius: BorderRadius.all(Radius.circular(5)),
          child: Container(
            width: deviceWidth * 0.5 - 12,
            height: deviceWidth * 0.25,
            child: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: MyColors.Black.withOpacity(0.45)),
              child: Center(
                child: Text(
                  text,
                  style: Theme.of(context).textTheme.display3,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          onTap: () {
            Navigator.of(context).push(PageRouteBuilder(
              pageBuilder: (_, __, ___) => MPProductListPage(
                type: PLPMode.Category,
                title: text,
                data: detail,
                model: model,
              ),
              transitionsBuilder:
                  (_, Animation<double> animation, __, Widget widget) {
                return Opacity(
                  opacity: animation.value,
                  child: widget,
                );
              },
              transitionDuration: Duration(milliseconds: 500),
            ));
          },
        ));
  }
}
