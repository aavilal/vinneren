import 'package:flutter/material.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:vinneren/models/enum-plpmode.dart';
import 'package:vinneren/pages/mp-product-list.dart';
import 'package:vinneren/scoped-models/main.dart';

class CategoryBoxWidget extends StatelessWidget {
  final MainModel model;
  final String text;
  final String urlImage;
  final dynamic detail;
  final bool isLeftAlign;
  CategoryBoxWidget({@required this.model, @required this.text, @required this.urlImage, @required this.detail, this.isLeftAlign});

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;

    return Ink.image(
        image: NetworkImage(urlImage),
        fit: BoxFit.cover,
        child: InkWell(
          highlightColor: MyColors.DarkBlue.withOpacity(0.4),
          splashColor: MyColors.DarkBlue.withOpacity(0.4),
          borderRadius: BorderRadius.all(Radius.circular(5)),
          child: Container(
            height: deviceWidth * 0.4,
            child: Row(
              mainAxisAlignment: isLeftAlign ? MainAxisAlignment.start : MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: deviceWidth * 0.5,
                  color: MyColors.DarkBlue.withOpacity(0.8),
                  padding: EdgeInsets.all(8),
                  child: Center(
                    child: Text(
                      text,
                      style: Theme.of(context).textTheme.display3,
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.of(context).push(PageRouteBuilder(
              pageBuilder: (_, __, ___) => MPProductListPage(
                type: PLPMode.Category,
                title: text,
                data: detail,
                model: model,
              ),
              transitionsBuilder:
                  (_, Animation<double> animation, __, Widget widget) {
                return Opacity(
                  opacity: animation.value,
                  child: widget,
                );
              },
              transitionDuration: Duration(milliseconds: 500),
            ));
          },
        ));
  }
}
