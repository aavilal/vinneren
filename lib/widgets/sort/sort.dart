import 'package:flutter/material.dart';
import 'package:vinneren/models/enum-sort.dart';
import 'package:vinneren/shared/theme.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'dart:async';

class SortWidget extends StatefulWidget {
  final SortMode sortMode;
  SortWidget({@required this.sortMode});

  @override
  State<StatefulWidget> createState() {
    return _SortWidgetState();
  }
}

class _SortWidgetState extends State<SortWidget> {
  SortMode _sortMode;
  bool _block;
  @override
  void initState() {
    _sortMode = widget.sortMode;
    _block = false;
    super.initState();
  }

  void _handleOnChange(SortMode sortMode, MainModel model) {
    if (!_block) {
      setState(() {
        _sortMode = sortMode;
      });
      model.setSort(sortMode);
      _block = true;
    }
    Timer(Duration(milliseconds: 500), () => Navigator.of(context).pop());
  }

  List<Widget> _buildOptions(MainModel model) {
    List<Widget> list = List<Widget>();
    list.add(InkWell(
      child: Row(
        children: <Widget>[
          Radio(
              value: SortMode.OrderByTopSaleDESC,
              groupValue: _sortMode,
              onChanged: (sortMode) => _handleOnChange(sortMode, model)),
          Text(
            'Best sellers',
            style: ThemeText.tsText9,
          )
        ],
      ),
      onTap: () => _handleOnChange(SortMode.OrderByTopSaleDESC, model),
    ));
    list.add(InkWell(
      child: Row(
        children: <Widget>[
          Radio(
              value: SortMode.OrderByPriceASC,
              groupValue: _sortMode,
              onChanged: (sortMode) => _handleOnChange(sortMode, model)),
          Text(
            'Price: low to high',
            style: ThemeText.tsText9,
          )
        ],
      ),
      onTap: () => _handleOnChange(SortMode.OrderByPriceASC, model),
    ));
    list.add(InkWell(
        child: Row(
          children: <Widget>[
            Radio(
                value: SortMode.OrderByPriceDESC,
                groupValue: _sortMode,
                onChanged: (sortMode) => _handleOnChange(sortMode, model)),
            Text(
              'Price: high to low',
              style: ThemeText.tsText9,
            )
          ],
        ),
        onTap: () => _handleOnChange(SortMode.OrderByPriceDESC, model)));
    list.add(InkWell(
      child: Row(
        children: <Widget>[
          Radio(
              value: SortMode.OrderByNameASC,
              groupValue: _sortMode,
              onChanged: (sortMode) => _handleOnChange(sortMode, model)),
          Text(
            'Title: A to Z',
            style: ThemeText.tsText9,
          )
        ],
      ),
      onTap: () => _handleOnChange(SortMode.OrderByNameASC, model),
    ));
    list.add(InkWell(
      child: Row(
        children: <Widget>[
          Radio(
              value: SortMode.OrderByNameDESC,
              groupValue: _sortMode,
              onChanged: (sortMode) => _handleOnChange(sortMode, model)),
          Text(
            'Title: Z to A',
            style: ThemeText.tsText9,
          )
        ],
      ),
      onTap: () => _handleOnChange(SortMode.OrderByNameDESC, model),
    ));
    list.add(InkWell(
      child: Row(
        children: <Widget>[
          Radio(
              value: SortMode.OrderByReleaseDateDESC,
              groupValue: _sortMode,
              onChanged: (sortMode) => _handleOnChange(sortMode, model)),
          Text(
            'New',
            style: ThemeText.tsText9,
          )
        ],
      ),
      onTap: () => _handleOnChange(SortMode.OrderByReleaseDateDESC, model),
    ));
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return AlertDialog(
        title: Text(
          'Sort By',
          style: Theme.of(context).textTheme.subhead,
        ),
        content: Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _buildOptions(model),
            ),
          ),
        ),
      );
    });
  }
}
