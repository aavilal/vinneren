import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/connected-model.dart';

class MainModel extends Model with ConnectedModel, UserModel, ProductModel, UtilityModel {
}
