import 'dart:async';
import 'package:scoped_model/scoped_model.dart';
import 'package:rxdart/subjects.dart';
import 'package:vinneren/models/enum-authmode.dart';
import 'package:vinneren/models/user.dart';
import 'package:vinneren/models/order-form.dart';
import 'package:vinneren/scoped-models/endpoints.dart';
import 'package:vinneren/shared/utils.dart';
import 'package:vinneren/models/product.dart';
import 'package:vinneren/models/taxonomy.dart';
import 'package:vinneren/models/enum-sort.dart';
import 'package:vinneren/models/filter.dart';
import 'package:vinneren/models/filter-option.dart';
import 'package:vinneren/models/enum-plp-view.dart';

mixin ConnectedModel on Model {
  String _email;
  String _token;
  dynamic _home;
  dynamic _categories;
  List<Taxonomy> _taxonomy;
  User _user;
  OrderForm _orderForm;
  bool _isLoading = false;
  List<Product> _products = new List();
  Map<String, dynamic> _page;
  SortMode _sort;
  List<Filter> _filters = new List();
  List<Filter> _filtersTMP = new List();
  double _maxPrice;
  double _minPrice;
  double _lowerValue;
  double _upperValue;
  double _lowerPriceFilter;
  double _upperPriceFilter;
  double _lowerValueTMP;
  double _upperValueTMP;
  double _lowerPriceFilterTMP;
  double _upperPriceFilterTMP;
  PLPViewMode _viewMode = PLPViewMode.Grid;
  PLPViewMode _viewModeTMP = PLPViewMode.Grid;
  final int _pageSize = 10;
  bool _isUpdate = false;
}

mixin UtilityModel on ConnectedModel {
  bool get isLoading {
    return _isLoading;
  }

  OrderForm get orderForm {
    return _orderForm;
  }

  User get user {
    return _user;
  }

  dynamic get home {
    return _home;
  }

  dynamic get categories {
    return _categories;
  }

  List<Taxonomy> get taxonomy {
    return _taxonomy;
  }
}

mixin ProductModel on ConnectedModel {
  BehaviorSubject<bool> _reloadPLPSubject = BehaviorSubject();

  void initReloadPLPSubject() {
    _reloadPLPSubject = BehaviorSubject();
  }
  BehaviorSubject<bool> get reloadPLPSubject {
    return _reloadPLPSubject;
  }

  List<Product> get products {
    return _products;
  }

  List<Filter> get filters {
    return _filtersTMP;
  }

  PLPViewMode get viewMode {
    return _viewModeTMP;
  }

  double get maxPrice {
    return _maxPrice;
  }

  double get minPrice {
    return _minPrice;
  }

  double get upperValue {
    return _upperValueTMP;
  }

  double get lowerValue {
    return _lowerValueTMP;
  }

  double get upperPriceFilter {
    return _upperPriceFilterTMP;
  }

  double get lowerPriceFilter {
    return _lowerPriceFilterTMP;
  }

  SortMode get sort {
    return _sort;
  }

  void setViewMode(PLPViewMode viewMode) {
    _viewModeTMP = viewMode;
    notifyListeners();
  }

  void setSort(SortMode sort) {
    _sort = sort;
    _reloadPLPSubject.add(true);
  }

  void setFilterSelected(Filter filter, FilterOption filterOption) {
    int index = _filtersTMP.indexOf(filter);
    if (index != -1) {
      _filtersTMP[index].filterSelected = filterOption;
      notifyListeners();
    }
    _isUpdate = true;
  }

  void resetSort() {
    _sort = SortMode.OrderByTopSaleDESC;
  }

  void setPriceLimits(double newLowerValue, double newUpperValue) {
    double lower = newLowerValue * _maxPrice / 100.0;
    double upper = newUpperValue * _maxPrice / 100.0;
    _lowerValueTMP = newLowerValue;
    _upperValueTMP = newUpperValue;
    _lowerPriceFilterTMP = lower;
    _upperPriceFilterTMP = upper;
    _isUpdate = true;
    notifyListeners();
  }

  void applyFilters() {
    _viewMode = _viewModeTMP;
    _lowerValue = _lowerValueTMP;
    _upperValue = _upperValueTMP;
    _lowerPriceFilter = _lowerPriceFilterTMP;
    _upperPriceFilter = _upperPriceFilterTMP;
    _filters = _filtersTMP.map((item) => new Filter.clone(item)).toList();
    if (_isUpdate)
      _reloadPLPSubject.add(true);
    else
      notifyListeners();
  }

  void getInitFilters() {
    _isUpdate = false;
    _viewModeTMP = _viewMode;
    _lowerValueTMP = _lowerValue;
    _upperValueTMP = _upperValue;
    _lowerPriceFilterTMP = _lowerPriceFilter;
    _upperPriceFilterTMP = _upperPriceFilter;
    _filtersTMP = _filters.map((item) => new Filter.clone(item)).toList();
    notifyListeners();
  }

  void resetFilters() {
    _viewModeTMP = PLPViewMode.Grid;
    _lowerValueTMP = 0.0;
    _upperValueTMP = 100.0;
    _lowerPriceFilterTMP = _minPrice;
    _upperPriceFilterTMP = _maxPrice;
    _filtersTMP.forEach((Filter filter) {
      filter.filterSelected = null;
    });
    _isUpdate = true;
    notifyListeners();
  }

  void resetProducts() {
    _products = new List();
    _page = {'start': 0, 'end': -1, 'total': 999999999999};
  }

  Future<Map<String, dynamic>> setSearch(String search, String params) async {
    List<Product> list = new List();
    Map<String, dynamic> data = {'search': search, 'params': params};
    Map<String, dynamic> response = await Endpoints.setSearch(data);
    if (!response['success']) {
      return response;
    }
    response['data'].forEach((dynamic element) {
      list.add(new Product.fromData(element));
    });
    response['data'] = list;
    return response;
  }

  Future<Map<String, dynamic>> setSearchPLP(
      String search, String params) async {
    if (_filters.length > 0) {
      _filters.forEach((Filter filter) {
        if (filter.filterSelected != null) {
          search = search + '/' + filter.filterSelected.value;
          params = params + ',' + filter.filterSelected.map;
        }
      });
    }

    if (_lowerPriceFilter != null &&
        _upperPriceFilter != null &&
        (_minPrice != _lowerPriceFilter || _maxPrice != _upperPriceFilter)) {
      params = params +
          '&fq=P:[' +
          _lowerPriceFilter.toString() +
          ' TO ' +
          _upperPriceFilter.toString() +
          ']';
    }

    if (_sort != null)
      params = params + '&O=' + _sort.toString().replaceAll('SortMode.', '');

    int start = _page['end'] + 1;
    int end = start + _pageSize - 1;
    params = params + '&_from=' + start.toString() + '&_to=' + end.toString();

    if (end > _page['total']) {
      return {'success': true};
    }

    _isLoading = true;
    notifyListeners();
    Map<String, dynamic> data = {'search': search, 'params': params};
    Map<String, dynamic> response = await Endpoints.setSearch(data);

    if (!response['success']) {
      return response;
    }
    response['data'].forEach((dynamic element) {
      _products.add(new Product.fromData(element));
    });

    if (_products.length == 0) _products = null;

    _page = response['page'];

    _isLoading = false;
    notifyListeners();
    return {'success': true};
  }

  void getFilters(String search, String params) async {
    _minPrice = 0.0;
    _maxPrice = 300000.0;
    _lowerValue = 0.0;
    _upperValue = 100.0;
    _lowerPriceFilter = _minPrice;
    _upperPriceFilter = _maxPrice;
    _viewMode = PLPViewMode.Grid;
    _viewModeTMP = PLPViewMode.Grid;
    _filters = new List();
    params = params +
        '&O=' +
        SortMode.OrderByPriceDESC.toString().replaceAll('SortMode.', '');
    params = params + '&_from=0&_to=0';
    Map<String, dynamic> dataMaxPrice = {'search': search, 'params': params};
    Map<String, dynamic> responseMaxPrice =
        await Endpoints.setSearch(dataMaxPrice);
    if (responseMaxPrice['success']) {
      responseMaxPrice['data'].forEach((dynamic element) {
        Product product = new Product.fromData(element);
        _maxPrice = product.price;
        _upperPriceFilter = _maxPrice;
      });
    }
    Map<String, dynamic> data = {'search': search, 'params': params};
    Map<String, dynamic> response = await Endpoints.getFacets(data);

    if (response['data']['Departments'] != null) {
      if (params.indexOf('?map=c') != -1) {
        dynamic elementFound;
        response['data']['Departments'].forEach((dynamic element) {
          if (Uri.encodeComponent(element['Name']) == search)
            elementFound = element;
        });
        if (elementFound != null)
          response['data']['Departments'].remove(elementFound);
      }
      _filters.add(
          new Filter.fromData('Categories', response['data']['Departments']));
    }
    if (response['data']['Brands'] != null) {
      _filters.add(new Filter.fromData('Brands', response['data']['Brands']));
    }
    if (response['data']['SpecificationFilters'] != null) {
      response['data']['SpecificationFilters'].forEach((key, value) {
        _filters.add(new Filter.fromData(key, value));
      });
    }
    getInitFilters();
  }
}

mixin UserModel on ConnectedModel {
  PublishSubject<AuthMode> _authModeSubject = PublishSubject();

  PublishSubject<AuthMode> get authModeSubject {
    return _authModeSubject;
  }

  Future<Map<String, dynamic>> autoAuthenticate() async {
    Map<String, dynamic> response = await Endpoints.getProfile();
    if (!response['success'] || !response['data']['IsUserDefined']) {
      _user = null;
      _authModeSubject.add(AuthMode.NotAuthenticated);
      //authMode = AuthMode.NotAuthenticated;
      return response;
    }
    _user = User.fromData(response['data']);

    response = await Endpoints.getOrderForm();
    if (!response['success']) {
      _user = null;
      _authModeSubject.add(AuthMode.NotAuthenticated);
      //authMode = AuthMode.NotAuthenticated;
      return response;
    }

    _orderForm = OrderForm.fromData(response['data']);

    response = await Endpoints.getResource('app-home');
    if (!response['success']) {
      _user = null;
      _orderForm = null;
      _authModeSubject.add(AuthMode.NotAuthenticated);
      //authMode = AuthMode.NotAuthenticated;
      return response;
    }

    _home = response['data'];

    response = await Endpoints.getResource('app-categories');
    if (!response['success']) {
      _user = null;
      _orderForm = null;
      _authModeSubject.add(AuthMode.NotAuthenticated);
      //authMode = AuthMode.NotAuthenticated;
      return response;
    }

    _categories = response['data'];

    response = await Endpoints.getTaxonomy();
    if (!response['success']) {
      _user = null;
      _orderForm = null;
      _authModeSubject.add(AuthMode.NotAuthenticated);
      //authMode = AuthMode.NotAuthenticated;
      return response;
    }

    _taxonomy = new List();
    response['data'].forEach((dynamic element) {
      _taxonomy.add(Taxonomy.fromData(element));
    });
    _authModeSubject.add(AuthMode.Authenticated);
    return {'success': true};
  }

  Future<Map<String, dynamic>> authenticate(String email) async {
    _isLoading = true;
    notifyListeners();
    _email = email;
    Map<String, dynamic> response = await Endpoints.getStart();
    if (!response['success']) {
      _isLoading = false;
      notifyListeners();
      return response;
    }
    _token = response['data']['authenticationToken'];
    Map<String, dynamic> data = {'email': _email, 'token': _token};
    response = await Endpoints.setSend(data);
    if (!response['success']) {
      _isLoading = false;
      notifyListeners();
      return response;
    }
    _isLoading = false;
    notifyListeners();
    return {'success': true};
  }

  Future<Map<String, dynamic>> verify(String code) async {
    _isLoading = true;
    notifyListeners();
    final Map<String, dynamic> data = {
      'email': _email,
      'token': _token,
      'key': code
    };
    Map<String, dynamic> response = await Endpoints.setValidate(data);
    if (!response['success']) {
      _user = null;
      _isLoading = false;
      notifyListeners();
      return response;
    }
    if (response['data']['authCookie'] == null) {
      _user = null;
      _isLoading = false;
      notifyListeners();
      return {'success': false, 'message': 'Invalid code.'};
    }

    response = await Endpoints.getSessions();
    if (!response['success']) {
      _user = null;
      _isLoading = false;
      notifyListeners();
      //authMode = AuthMode.NotAuthenticated;
      return response;
    }
    response = await autoAuthenticate();
    _isLoading = false;
    notifyListeners();
    return response;
  }

  void logout() async {
    _user = null;
    _authModeSubject.add(AuthMode.NotAuthenticated);
    await Utils.removeCookie();
    //authMode = AuthMode.NotAuthenticated;
  }
}
