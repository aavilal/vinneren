import 'dart:convert';
import 'package:vinneren/shared/utils.dart';
import 'package:http/http.dart' as http;

class Endpoints {
  static final String _host = 'http://ec2-3-86-161-98.compute-1.amazonaws.com:9000';
  static String _cookie;
  static http.Response _response;
  static Map<String, dynamic> _responseBody;
  static Map<String, dynamic> _responseHeaders;

  static Future<Map<String, dynamic>> getStart() async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.get(
        _host + '/api/authentication/start',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in authentication/start'};

      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in authentication/start'};
    }
  }

  static Future<Map<String, dynamic>> setSend(Map<String, dynamic> data) async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.post(
        _host + '/api/authentication/accesskey/send',
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in authentication/accesskey/send'};

      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in authentication/accesskey/send'};
    }
  }

  static Future<Map<String, dynamic>> setValidate(Map<String, dynamic> data) async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.post(
        _host + '/api/authentication/accesskey/validate',
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in authentication/accesskey/validate'};

      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in authentication/accesskey/validate'};
    }
  }

  static Future<Map<String, dynamic>> getSessions() async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.get(
        _host + '/api/authentication/sessions',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in authentication/sessions'};

      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in authentication/sessions'};
    }
  }

  static Future<Map<String, dynamic>> getProfile() async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.get(
        _host + '/api/authentication/get-profile',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in authentication/get-profile'};

      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in authentication/get-profile'};
    }
  }

  static Future<Map<String, dynamic>> getOrderForm() async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.get(
        _host + '/api/checkout/orderform',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in checkout/orderform'};

      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in checkout/orderform'};
    }
  }

  static Future<Map<String, dynamic>> getResource(String resource) async {
    try {
      _response = await http.get(
        _host + '/api/file/' + resource,
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in file/' + resource};

      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in file/' + resource};
    }
  }

  static Future<Map<String, dynamic>> getTaxonomy() async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.get(
        _host + '/api/catalog/category',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in catalog/category'};

      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in catalog/category'};
    }
  }

  static Future<Map<String, dynamic>> setSearch(Map<String, dynamic> data) async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.post(
        _host + '/api/product/search',
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in product/search'};
      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in product/search'};
    }
  }

  static Future<Map<String, dynamic>> getFacets(Map<String, dynamic> data) async {
    try {
      _cookie = await Utils.getCookie();
      _response = await http.post(
        _host + '/api/facets/search',
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'cookie': _cookie
        },
      );
      _responseBody = json.decode(_response.body);
      _responseHeaders = _response.headers;
      Utils.setCookie(_responseHeaders['set-cookie']);
      if (!_responseBody['success'])
        return {'success': false, 'message': 'Error in facets/search'};
      return _responseBody;
    } catch (e) {
      return {'success': false, 'message': 'Error in facets/search'};
    }
  }
}
