import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class Utils {

  static Future<String> getCookie() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    Set<String> keys = prefs.getKeys();
    String cookie = '';
    keys.forEach((String key) {
      if (key.indexOf('vinneren_') != -1) {
        cookie = cookie + prefs.get(key) + ';';
      }
    });
    return cookie;
  }

  static void setCookie(String headerCookies) async {
    if (headerCookies != null) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      List cookies = json.decode(headerCookies);
      cookies.forEach((cookie) {
        String cookieTmp = cookie.split(';')[0];
        String cookieKeyTmp = cookieTmp.split('=')[0];
        prefs.setString('vinneren_' + cookieKeyTmp, cookieTmp);
      });
    }
  }

  static Future<bool> removeCookie() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    Set<String> keys = prefs.getKeys();
    keys.forEach((String key) {
      if (key.indexOf('vinneren_') != -1) {
        prefs.remove(key);
      }
    });
    return true;
  }
}
