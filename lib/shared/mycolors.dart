import 'package:flutter/material.dart';

class MyColors {
  MyColors._(); // this basically makes it so you can instantiate this class
  static const Color DarkBlue = const Color(0xFF0D47A1);
  static const Color LightBlue = const Color(0xFF3185FC);
  static const Color GreyBrown = const Color(0xFF434343);
  static const Color WarmGrey = const Color(0xFF919191);
  static const Color White = const Color(0xFFFFFFFF);
  static const Color Black = const Color(0xFF000000);
  static const Color Grey = const Color(0xFF656565);
  static const Color LightGrey = const Color(0xFFE5E5E5);
  static const Color LighterGrey = const Color(0xFFF2F2F2);
  static const Color Orange = const Color(0xFFFFA500);
  static const Color Green = const Color(0xFF52B640);
  static const Color Red = const Color(0xFFFF0000);

  static const _bluePrimaryValue = 0xFF0D47A1;

  static const MaterialColor blue = const MaterialColor(
    _bluePrimaryValue,
    const <int, Color>{
      50: Color(_bluePrimaryValue),
      100: Color(_bluePrimaryValue),
      200: Color(_bluePrimaryValue),
      300: Color(_bluePrimaryValue),
      400: Color(_bluePrimaryValue),
      500: Color(_bluePrimaryValue),
      600: Color(_bluePrimaryValue),
      700: Color(_bluePrimaryValue),
      800: Color(_bluePrimaryValue),
      900: Color(_bluePrimaryValue),
    },
  );
}
