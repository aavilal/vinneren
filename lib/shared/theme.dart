import 'package:flutter/material.dart';
import 'package:vinneren/shared/mycolors.dart';

final ThemeData _lightTheme = ThemeData(
    fontFamily: 'NunitoSans',
    brightness: Brightness.light,
    primarySwatch: MyColors.blue,
    accentColor: MyColors.blue,
    buttonColor: MyColors.blue,
    cursorColor: MyColors.blue,
    textSelectionColor: MyColors.LightBlue,
    textSelectionHandleColor: MyColors.DarkBlue,
    backgroundColor: MyColors.White,
    //dialogTheme: ,
    textTheme: TextTheme(
      button: TextStyle(
        color: MyColors.White,
        fontWeight: FontWeight.w600,
        fontSize: 16.0,
      ),
      headline: TextStyle(
        color: MyColors.White,
        fontWeight: FontWeight.w600,
        fontSize: 24.0,
      ),
      subhead: TextStyle(
        color: MyColors.GreyBrown,
        fontWeight: FontWeight.w600,
        fontSize: 18.0,
      ),
      title: TextStyle(
        color: MyColors.GreyBrown,
        fontWeight: FontWeight.w800,
        fontSize: 20.0,
      ),
      display1: TextStyle(
        color: MyColors.White,
        fontWeight: FontWeight.w800,
        fontSize: 24.0,
      ),
      display2: TextStyle(
        color: MyColors.White,
        fontWeight: FontWeight.w600,
        fontSize: 14.0,
      ),
      display3: TextStyle(
        color: MyColors.White,
        fontWeight: FontWeight.w800,
        fontSize: 18.0,
      ),
      overline: TextStyle(
        color: MyColors.White,
        fontWeight: FontWeight.w600,
        fontSize: 12.0,
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
        hintStyle: TextStyle(
          color: MyColors.WarmGrey,
          fontWeight: FontWeight.w600,
          fontSize: 16.0,
        ),
        labelStyle: TextStyle(
          color: MyColors.WarmGrey,
          fontWeight: FontWeight.w600,
          fontSize: 16.0,
        )));

ThemeData getThemeData(context) {
  return _lightTheme;
}

class ThemeText {

  static TextStyle tsText1 = TextStyle(
      color: MyColors.GreyBrown,
      fontWeight: FontWeight.w600,
      fontSize: 14.0,
      height: 0.85
  );

  static TextStyle tsText2 = TextStyle(
    color: MyColors.WarmGrey,
    fontWeight: FontWeight.w600,
    decoration: TextDecoration.lineThrough,
    fontSize: 12.0,
  );

  static TextStyle tsText3 = TextStyle(
    color: MyColors.LightBlue,
    fontWeight: FontWeight.w800,
    fontSize: 16.0,
  );

  static TextStyle tsText4 = TextStyle(
    color: MyColors.WarmGrey,
    fontWeight: FontWeight.w600,
    fontSize: 12.0,
  );

  static TextStyle tsText5 = TextStyle(
    color: MyColors.GreyBrown,
    fontWeight: FontWeight.w600,
    fontSize: 12.0,
  );

  static TextStyle tsText6 = TextStyle(
    color: MyColors.DarkBlue,
    fontWeight: FontWeight.w600,
    fontSize: 14.0,
  );

  static TextStyle tsText7 = TextStyle(
    color: MyColors.DarkBlue,
    fontWeight: FontWeight.w600,
    fontSize: 12.0,
  );

  static TextStyle tsText8 = TextStyle(
    color: MyColors.DarkBlue,
    fontWeight: FontWeight.w800,
    fontSize: 24.0,
  );

  static TextStyle tsText9 = TextStyle(
    color: MyColors.GreyBrown,
    fontWeight: FontWeight.w600,
    fontSize: 16.0,
  );

  static TextStyle tsText10 = TextStyle(
    color: MyColors.GreyBrown,
    fontWeight: FontWeight.w600,
    fontSize: 14.0,
  );

  static TextStyle tsText11 = TextStyle(
    color: MyColors.LightBlue,
    fontWeight: FontWeight.w600,
    fontSize: 14.0,
  );

  static TextStyle tsText12 = TextStyle(
    color: MyColors.DarkBlue,
    fontWeight: FontWeight.w600,
    fontSize: 16.0,
  );

  static TextStyle tsText13 = TextStyle(
    color: MyColors.LightBlue,
    fontWeight: FontWeight.w800,
    fontSize: 18.0,
  );

}
