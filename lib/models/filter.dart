import 'package:vinneren/models/filter-option.dart'
;
class Filter {
  String name;
  FilterOption filterSelected;
  List<FilterOption> filterOptions;
  Filter.fromData(String name, List<dynamic> data) {
    this.name = name;
    this.filterOptions = new List();
    data.forEach((dynamic element) {
      this.filterOptions.add(new FilterOption.fromData(element));
    });
  }

  Filter.clone(Filter source) {
    this.name = source.name;
    this.filterSelected = source.filterSelected != null ? new FilterOption.clone(source.filterSelected) : null;
    this.filterOptions = source.filterOptions.map((item) => new FilterOption.clone(item)).toList();
  }
}
