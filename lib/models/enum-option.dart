enum Option {
  Profile,
  Marketplace,
  Categories,
  Orders,
  Cart,
  Search,
  Wallet,
  PaymentServices,
  AboutVinneren,
  Help,
  Logout
}