enum SortMode {
  OrderByTopSaleDESC,
  OrderByPriceASC,
  OrderByPriceDESC,
  OrderByNameASC,
  OrderByNameDESC,
  OrderByReleaseDateDESC
}