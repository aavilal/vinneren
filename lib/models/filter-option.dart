class FilterOption {
  String name;
  String map;
  String value;
  FilterOption.fromData(dynamic data) {
    this.name = data['Name'].toString().toUpperCase();
    this.map = data['Map'];
    this.value = data['Value'];
  }

  FilterOption.clone(FilterOption source) {
    this.name = source.name;
    this.map = source.map;
    this.value = source.value;
  }
}
