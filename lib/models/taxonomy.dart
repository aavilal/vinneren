class Taxonomy {
  int id;
  String name;
  List<Taxonomy> children;
  Taxonomy.fromData(dynamic data) {
    this.id = data['id'];
    this.name = data['name'];
    if(data['children'].length > 0) {
      this.children = new List();
      data['children'].forEach((dynamic element) {
        this.children.add(Taxonomy.fromData(element));
      });
    }
  }
}
