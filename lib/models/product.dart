class Product {
  String id;
  String image;
  String title;
  String description;
  double price;
  double discountPrice;
  String seller;
  int stock;

  Product.fromData(dynamic data) {
    if (data['productId'] == '31034581')
      print('XXXXX' + data.toString());
    this.id = data['productId'];
    this.image = data['items'][0]['images'][0]['imageUrl'];
    this.title = data['productName'];
    this.description = data['description'];
    this.price = data['items'][0]['sellers'][0]['commertialOffer']['ListPrice'] * 1.0;
    this.discountPrice = data['items'][0]['sellers'][0]['commertialOffer']['Price'] * 1.0;
    this.seller = data['items'][0]['sellers'][0]['sellerName'];
    this.stock = data['items'][0]['sellers'][0]['commertialOffer']['AvailableQuantity'];
  }
}
