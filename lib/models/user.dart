class User {
  String id;
  String email;
  String firstName;
  String lastName;

  //User({@required this.id, @required this.email, @required this.firstName, @required this.lastName});
  User.fromData(dynamic data) {
    this.id = data['UserId'];
    email = data['Email'];
    firstName = data['FirstName'];
    lastName = data['LastName'];
  }
}
