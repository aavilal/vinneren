
class OrderForm {
  String orderFormId;
  String salesChannel;
  bool loggedIn;

  //OrderForm({@required this.orderFormId, @required this.salesChannel, @required this.loggedIn});
  OrderForm.fromData(dynamic data) {
    this.orderFormId = data['orderFormId'];
    salesChannel = data['salesChannel'];
    loggedIn = data['loggedIn'];
  }
}
