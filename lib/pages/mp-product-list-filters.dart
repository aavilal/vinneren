import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:vinneren/shared/theme.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:vinneren/models/enum-plp-view.dart';
import 'package:vinneren/models/filter.dart';
import 'package:vinneren/widgets/filter/filter.dart';

class MPProductListFiltersPage extends StatefulWidget {
  final MainModel model;
  MPProductListFiltersPage({@required this.model});

  @override
  State<StatefulWidget> createState() {
    return _MPProductListFiltersPageState();
  }
}

class _MPProductListFiltersPageState extends State<MPProductListFiltersPage> {

  @override
  void initState() {
    widget.model.getInitFilters();
    super.initState();
  }

  Widget _buildOtherFilter(BuildContext context, Filter filter) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(right: 16.0, left: 16.0, top: 8.0, bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: deviceWidth * 0.6 - 16.0,
            child: Text(
              filter.name,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: ThemeText.tsText9,
            ),
          ),
          Container(
            width: deviceWidth * 0.4 - 16.0,
            alignment: Alignment.centerRight,
            child:  InkWell(child: Text(
              filter.filterSelected != null
                  ? filter.filterSelected.name.toUpperCase()
                  : 'SELECT',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: ThemeText.tsText11,
              textAlign: TextAlign.right,
            ), onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return FilterWidget(filter: filter);
                  });
            },),
          ),
        ],
      ),
    );
  }

  Widget _buildView(BuildContext context, MainModel model) {
    return Container(
      width: double.infinity,
      padding:
          EdgeInsets.only(right: 16.0, left: 16.0, top: 24.0, bottom: 12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              'View',
              style: ThemeText.tsText9,
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child: RaisedButton(
                    color: model.viewMode == PLPViewMode.Grid ? MyColors.LightBlue : MyColors.White,
                    padding:
                        EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
                    shape: StadiumBorder(),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.dashboard,
                          color: model.viewMode == PLPViewMode.Grid ? MyColors.White : MyColors.GreyBrown,
                        ),
                        SizedBox(
                          width: 8.0,
                        ),
                        Text(
                          'Grid',
                          style: model.viewMode == PLPViewMode.Grid ? Theme.of(context).textTheme.display2 : ThemeText.tsText10,
                        )
                      ],
                    ),
                    onPressed: () => {
                      model.setViewMode(PLPViewMode.Grid)
                    },
                  ),
                ),
                SizedBox(
                  width: 16.0,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child: RaisedButton(
                    padding:
                        EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
                    color: model.viewMode == PLPViewMode.Large ? MyColors.LightBlue : MyColors.White,
                    shape: StadiumBorder(),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.view_agenda,
                          color: model.viewMode == PLPViewMode.Large ? MyColors.White : MyColors.GreyBrown,
                        ),
                        SizedBox(
                          width: 8.0,
                        ),
                        Text(
                          'Large',
                          style: model.viewMode == PLPViewMode.Large ? Theme.of(context).textTheme.display2 : ThemeText.tsText10,
                        )
                      ],
                    ),
                    onPressed: () => {
                      model.setViewMode(PLPViewMode.Large)
                    },
                  ),
                ),
                SizedBox(
                  width: 16.0,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child: RaisedButton(
                    padding:
                        EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
                    color: model.viewMode == PLPViewMode.List ? MyColors.LightBlue : MyColors.White,
                    shape: StadiumBorder(),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.view_list,
                          color: model.viewMode == PLPViewMode.List ? MyColors.White : MyColors.GreyBrown,
                        ),
                        SizedBox(
                          width: 8.0,
                        ),
                        Text(
                          'List',
                          style: model.viewMode == PLPViewMode.List ? Theme.of(context).textTheme.display2 : ThemeText.tsText10,
                        )
                      ],
                    ),
                    onPressed: () => {
                      model.setViewMode(PLPViewMode.List)
                    },
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildPriceFilter(BuildContext context, MainModel model) {
    return Container(
        width: double.infinity,
        padding:
            EdgeInsets.only(right: 8.0, left: 8.0, top: 12.0, bottom: 12.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 8.0),
                child: Text(
                  'Price',
                  style: ThemeText.tsText9,
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 24.0, bottom: 8.0),
                child: SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                        thumbShape:
                            RoundSliderThumbShape(enabledThumbRadius: 10.0),
                        overlayColor: MyColors.LightBlue,
                        activeTickMarkColor: Colors.transparent,
                        activeTrackColor: MyColors.LightBlue,
                        inactiveTrackColor: MyColors.LightBlue.withOpacity(0.5),
                        thumbColor: MyColors.LightBlue,
                        valueIndicatorColor: MyColors.LightBlue,
                        showValueIndicator: ShowValueIndicator.always),
                    child: RangeSlider(
                      min: 0.0,
                      max: 100.0,
                      lowerValue: model.lowerValue,
                      upperValue: model.upperValue,
                      divisions: 1000,
                      showValueIndicator: true,
                      valueIndicatorMaxDecimals: 2,
                      valueIndicatorFormatter: (int index, double value) {
                        return FlutterMoneyFormatter(
                                    amount: value * model.maxPrice / 100.0)
                                .output
                                .symbolOnLeft +
                            ' MXN';
                      },
                      onChanged: (double newLowerValue, double newUpperValue) {
                        model.setPriceLimits(newLowerValue, newUpperValue);
                      },
                    )),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        FlutterMoneyFormatter(amount: model.lowerPriceFilter)
                                .output
                                .symbolOnLeft +
                            ' MXN',
                        style: ThemeText.tsText10,
                      ),
                      Text(
                        FlutterMoneyFormatter(amount: model.upperPriceFilter)
                                .output
                                .symbolOnLeft +
                            ' MXN',
                        style: ThemeText.tsText10,
                      )
                    ]),
              )
            ]));
  }

  List<Widget> _buildFilters(BuildContext context, MainModel model) {
    List<Widget> list = new List();
    list.add(_buildView(context, model));
    list.add(Divider(color: MyColors.WarmGrey));
    list.add(_buildPriceFilter(context, model));
    model.filters.forEach((Filter filter) {
      list.add(Divider(color: MyColors.WarmGrey));
      list.add(_buildOtherFilter(context, filter));
    });
    return list;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return Scaffold(
        backgroundColor: MyColors.White,
        appBar: AppBar(
            title: Text('Filters'),
            toolbarOpacity: 1,
            elevation: 2.0,
            actions: <Widget>[
              Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
                  child: FlatButton(
                    child: Text(
                      'CLEAR',
                      style: Theme.of(context).textTheme.display2,
                    ),
                    onPressed: () {
                      model.resetFilters();
                    },
                  )),
            ]),
        body: Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: _buildFilters(context, model),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.only(bottom: 8.0, left: 16.0, right: 16.0),
              child: RaisedButton(
                padding: EdgeInsets.all(12.0),
                shape: StadiumBorder(),
                child: Text(
                  'Apply',
                  style: Theme.of(context).textTheme.button,
                ),
                onPressed: () {
                  model.applyFilters();
                  Navigator.of(context).pop();
                },
              ),
            )
          ],
        ),
      );
    });
  }
}
