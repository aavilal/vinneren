import 'package:flutter/material.dart';
import 'package:vinneren/widgets/banner/static-banner.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/models/enum-plpmode.dart';
import 'package:vinneren/models/enum-appmode.dart';
import 'package:vinneren/widgets/header/header.dart';
import 'package:vinneren/widgets/product/product-grid.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'dart:core';

class MPProductListPage extends StatefulWidget {
  final PLPMode type;
  final String title;
  final dynamic data;
  final MainModel model;
  MPProductListPage(
      {@required this.type, @required this.title, @required this.data, @required this.model});

  @override
  State<StatefulWidget> createState() {
    return _MPProductListPageState();
  }
}

class _MPProductListPageState extends State<MPProductListPage> {
  ScrollController _scrollController;
  double _offset = 0.0;
  bool _block;
  List<Widget> _buildWidgets(MainModel model) {
    List<Widget> list = new List();
    if (widget.type == PLPMode.Category) {
      if (widget.data['image'] != null && widget.data['image'].length > 0) {
        list.add(StaticBannerWidget(
          urlImage: widget.data['image'],
        ));
      }
      //add subcategories
    }
    if (widget.type == PLPMode.Cluster) {
      if (widget.data['image'] != null && widget.data['image'].length > 0) {
        list.add(StaticBannerWidget(
          urlImage: widget.data['image'],
        ));
      }
    }
    if (widget.type == PLPMode.Search) {
      //add search input
    }

    list.add(ProductGridWidget(
      keyWidget: widget.type.toString()
    ));
    return list;
  }

  @override
  void initState() {
    super.initState();
    _block = false;
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    widget.model.resetProducts();
    widget.model.resetSort();
    _getFilters();
    _getData();
    widget.model.initReloadPLPSubject();
    widget.model.reloadPLPSubject.listen((bool reloadEvent) {
      widget.model.resetProducts();
      _getData();
      if (_offset > 0)
        _scrollController.animateTo(0,
            curve: Curves.linear, duration: Duration(milliseconds: 500));
    });
  }

  void _getFilters() {
    String params = '';
    String search = '';
    if (widget.type == PLPMode.Category) {
      search = Uri.encodeComponent(widget.title);
      params = '?map=c';
    }
    if (widget.type == PLPMode.Cluster) {
      search = widget.data['clusterId'].toString();
      params = '?map=productClusterIds';
    }
    if (widget.type == PLPMode.Search) {
      search = Uri.encodeComponent('some text');
      params = '?map=ft';
    }
    widget.model.getFilters(search, params);
  }

  void _getData() async {
    _block = true;
    String params = '';
    String search = '';
    if (widget.type == PLPMode.Category) {
      search = Uri.encodeComponent(widget.title);
      params = '?map=c';
    }
    if (widget.type == PLPMode.Cluster) {
      params = '?fq=productClusterIds:' + widget.data['clusterId'].toString();
    }
    if (widget.type == PLPMode.Search) {
      params = '?ft=' + Uri.encodeComponent('some text');
    }
    await widget.model.setSearchPLP(search, params);
    _block = false;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    widget.model.reloadPLPSubject.close();
    super.dispose();
  }

  _scrollListener() {
    setState(() {
      _offset = _scrollController.offset;
    });
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      if (!_block)
        _getData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return Scaffold(
        backgroundColor: MyColors.White,
        appBar: HeaderWidget(
          appMode: AppMode.Marketplace,
          title: widget.title,
        ).getAppBar(context),
        body: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            children: _buildWidgets(model),
          ),
          //_buildFixedBar(),
        ),
      );
    });
  }
}
