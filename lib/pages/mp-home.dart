import 'package:flutter/material.dart';
import 'package:vinneren/widgets/banner/banner.dart';
import 'package:vinneren/widgets/boxbanner/box-banner.dart';
import 'package:vinneren/widgets/category/category-list.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/widgets/product/product-list.dart';


class MPHomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _MPHomePageState();
  }
}

class _MPHomePageState extends State<MPHomePage> {

  List<Widget> _buildWidgets(MainModel model) {
    List<Widget> list = new List();
    model.home.forEach((key, value) {
      if (key.substring(key.indexOf('_') + 1) == 'banner') {
        list.add(BannerWidget(model: model, data: value));
      }
      if (key.substring(key.indexOf('_') + 1) == 'boxBanner') {
        list.add(BoxBannerWidget(model: model, data: value));
      }
      if (key.substring(key.indexOf('_') + 1) == 'categories') {
        list.add(CategoryListWidget(model: model, data: value,));
      }
      if (key.substring(key.indexOf('_') + 1) == 'productList') {
        list.add(ProductListWidget(model: model, data: value, keyWidget: key));
      }
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return Column(
            children: _buildWidgets(model),
          );
        });
  }
}
