import 'package:flutter/material.dart';
import 'package:vinneren/shared/mycolors.dart';

class SplashPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                MyColors.LightBlue,
                MyColors.DarkBlue,
                MyColors.DarkBlue
              ],
              begin: FractionalOffset.topLeft,
              end: FractionalOffset.bottomRight),
        ),
        padding: EdgeInsets.symmetric(horizontal: 50),
        child: Center(
          child: Image.asset('assets/logo/vinneren.png'),
        ),
      ),
    );
  }
}
