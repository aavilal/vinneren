import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/models/enum-appmode.dart';
import 'package:vinneren/models/product.dart';
import 'package:vinneren/widgets/header/header.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'dart:core';

class MPProductDetailPage extends StatefulWidget {
  final Product product;
  MPProductDetailPage({@required this.product});

  @override
  State<StatefulWidget> createState() {
    return _MPProductDetailPageState();
  }
}

class _MPProductDetailPageState extends State<MPProductDetailPage> {

  @override
  Widget build(BuildContext context) {
    //print(widget.product.toString());
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return Scaffold(
            backgroundColor: MyColors.White,
            appBar: HeaderWidget(
              appMode: AppMode.Marketplace,
              title: 'Product detail',
            ).getAppBar(context),
            body: Container(),
          );
        });
  }
}
