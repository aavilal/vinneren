import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/widgets/wave/config.dart';
import 'package:vinneren/widgets/wave/wave.dart';
import 'package:vinneren/widgets/loader/adapative_progress_indicator.dart';
import 'package:vinneren/widgets/helper/ensure-visible.dart';
import 'package:vinneren/widgets/alert/alert.dart';
import 'package:vinneren/shared/mycolors.dart';
import 'package:vinneren/pages/home.dart';
import 'package:vinneren/models/enum-appmode.dart';

class VerifyPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _VerifyPageState();
  }
}

class _VerifyPageState extends State<VerifyPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _inputFocusNode = FocusNode();
  String _codeInput;

  Widget _buildBackground(deviceWidth, deviceHeight) {
    return Container(
      height: deviceHeight,
      width: deviceWidth,
      child: Container(
        child: WaveWidget(
          config: CustomConfig(
            colors: [
              Colors.white70,
              Colors.white54,
              Colors.white30,
              Colors.white,
            ],
            durations: [32000, 21000, 18000, 5000],
            heightPercentages: [0.2, 0.21, 0.23, 0.26],
          ),
          backgroundColor: MyColors.DarkBlue,
          size: Size(double.infinity, double.infinity),
          waveAmplitude: 0,
        ),
      ),
    );
  }

  Widget _buildLogoHeader(deviceWidth, deviceHeight) {
    return Container(
      width: deviceWidth,
      height: deviceHeight * 0.2,
      child: Center(
        child: Image.asset(
          'assets/logo/vinneren.png',
          width: deviceWidth * 0.45,
        ),
      ),
    );
  }

  Widget _buildTitleHeader(deviceWidth, deviceHeight) {
    return Positioned(
      top: deviceHeight * 0.2,
      child: Container(
        width: deviceWidth,
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Center(
          child: Text(
            'Almost there!',
            style: Theme.of(context).textTheme.headline,
          ),
        ),
      ),
    );
  }

  Widget _buildForm(BuildContext context, deviceWidth, deviceHeight) {
    return Positioned(
        top: deviceHeight * 0.4,
        child: Container(
            width: deviceWidth,
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Form(
              key: _formKey,
              child: Column(children: <Widget>[
                Text(
                  'Check your email and enter the code we sent you to sign in',
                  style: Theme.of(context).textTheme.subhead,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 12.0),
                _buildEmailTextField(),
                SizedBox(height: 32.0),
                _buildSubmitButton()
              ]),
            )));
  }

  Widget _buildEmailTextField() {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return EnsureVisible(
            focusNode: _inputFocusNode,
            child: TextFormField(
              focusNode: _inputFocusNode,
              decoration: InputDecoration(
                labelText: 'Code',
                labelStyle: Theme.of(context).inputDecorationTheme.labelStyle,
                filled: true,
                fillColor: Colors.white,
                hintText: 'Enter your code',
                hintStyle: Theme.of(context).inputDecorationTheme.hintStyle,
              ),
              keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false),
              textInputAction: TextInputAction.done,
              onFieldSubmitted: (value) {
                _inputFocusNode.unfocus();
                _submitForm(model);
              },
              validator: (String value) {
                if (value.isEmpty || !RegExp(r"^[0-9]*$").hasMatch(value)) {
                  return 'Please enter a valid code';
                }
              },
              onSaved: (String value) {
                _codeInput = value;
              },
            ),
          );
        });
  }

  Widget _buildSubmitButton() {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return model.isLoading
              ? AdaptiveProgressIndicator()
              : Container(
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.all(12.0),
              shape: StadiumBorder(),
              child: Text(
                'Sign in',
                style: Theme.of(context).textTheme.button,
              ),
              onPressed: () => _submitForm(model),
            ),
          );
        });
  }

  void _submitForm(MainModel model) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    Map<String, dynamic> successInformation =
    await model.verify(_codeInput);

    if (successInformation['success']) {
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (_, __, ___) => HomePage(model: model, appMode: AppMode.Marketplace),
        transitionsBuilder:
            (_, Animation<double> animation, __, Widget widget) {
          return Opacity(
            opacity: animation.value,
            child: widget,
          );
        },
        transitionDuration: Duration(milliseconds: 1000),
      ));

    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertWidget(
              title: 'Uuups something went wrong!',
              messages: [successInformation['message']],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              _buildBackground(deviceWidth, deviceHeight),
              _buildLogoHeader(deviceWidth, deviceHeight),
              _buildTitleHeader(deviceWidth, deviceHeight),
              _buildForm(context, deviceWidth, deviceHeight),
            ],
          ),
        ),
      ),
    );
  }
}
