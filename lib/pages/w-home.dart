import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';

class WHomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _WHomePageState();
  }
}

class _WHomePageState extends State<WHomePage> {

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return Container(
            child: Center(child: Text('WALLET PAGE'),),
          );
        });
  }
}
