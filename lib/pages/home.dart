import 'package:flutter/material.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/models/enum-appmode.dart';
import 'package:vinneren/widgets/header/header.dart';
import 'package:vinneren/widgets/drawer/drawer.dart';
import 'package:vinneren/models/enum-option.dart';
import 'package:vinneren/widgets/navigationbar/fancy-bottom-navigation.dart';
import 'package:vinneren/pages/mp-home.dart';
import 'package:vinneren/pages/w-home.dart';
import 'package:vinneren/pages/ps-home.dart';
import 'package:vinneren/shared/mycolors.dart';

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  AppMode appMode;
  MainModel model;
  HomePage({@required this.model, @required this.appMode});
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  AppMode _appMode;
  int currentPage = 0;
  GlobalKey bottomNavigationKey = GlobalKey();

  @override
  void initState() {
    _appMode = widget.appMode;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final FancyBottomNavigationState fState = bottomNavigationKey.currentState;
    if (fState != null) {
      if ((_appMode == AppMode.Marketplace && fState.currentSelected != 0) ||
          (_appMode == AppMode.Wallet && fState.currentSelected != 1) ||
          (_appMode == AppMode.Wallet && fState.currentSelected != 2))
        fState.setPage(_appMode == AppMode.Marketplace
            ? 0
            : (_appMode == AppMode.Wallet ? 1 : 2));
    }
    if (_appMode == AppMode.Marketplace) {
      currentPage = 0;
    }

    if (_appMode == AppMode.Wallet) {
      currentPage = 1;
    }

    if (_appMode == AppMode.PaymentServices) {
      currentPage = 2;
    }
    return Scaffold(
      backgroundColor: MyColors.White,
      appBar: HeaderWidget(
        appMode: _appMode,
        title: _appMode == AppMode.Marketplace
            ? 'Marketplace'
            : _appMode == AppMode.Wallet ? 'Wallet' : 'Services',
      ).getAppBar(context),
      drawer: DrawerWidget(
          option: _appMode == AppMode.Marketplace
              ? Option.Marketplace
              : _appMode == AppMode.Wallet
              ? Option.Wallet
              : Option.PaymentServices)
          .getDrawer(context),
      body: Container(
          child: SingleChildScrollView(
            child: _appMode == AppMode.Marketplace
                ? MPHomePage()
                : _appMode == AppMode.Wallet ? WHomePage() : PSHomePage(),
          )),
      bottomNavigationBar: FancyBottomNavigation(
          tabs: [
            TabData(iconData: Icons.home, title: 'Marketplace'),
            TabData(iconData: Icons.payment, title: 'Wallet'),
            TabData(iconData: Icons.apps, title: 'Services')
          ],
          initialSelection: currentPage,
          key: bottomNavigationKey,
          onTabChangedListener: (position) {
            setState(() {
              _appMode = position == 0
                  ? AppMode.Marketplace
                  : position == 1
                  ? AppMode.Wallet
                  : AppMode.PaymentServices;
              currentPage = position;
            });
          }),
    );
  }
}
