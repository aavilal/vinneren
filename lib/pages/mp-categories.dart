import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vinneren/scoped-models/main.dart';
import 'package:vinneren/widgets/header/header.dart';
import 'package:vinneren/models/enum-appmode.dart';
import 'package:vinneren/widgets/category/category-box.dart';
import 'package:vinneren/models/taxonomy.dart';
import 'package:vinneren/shared/mycolors.dart';

class MPCategoriesPage extends StatelessWidget {

  List<Widget> _buildCategories(MainModel model) {
    List<Widget> list = new List();
    Taxonomy taxonomyFound;
    int index = 0;
    model.categories['data'].forEach((dynamic element) {
      taxonomyFound = model.taxonomy.firstWhere(
              (elementTaxonomy) => elementTaxonomy.id == element['categoryId'],
          orElse: () => null);
      dynamic detail = element['detail'];
      detail['categoryId'] = element['categoryId'];
      if (taxonomyFound != null) {
        list.add(CategoryBoxWidget(
          model: model, text: taxonomyFound.name, urlImage: element['image'], detail: detail, isLeftAlign: !index.isEven,));
      }
      index = index + 1;
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return Scaffold(
        backgroundColor: MyColors.White,
        appBar: HeaderWidget(
          appMode: AppMode.Marketplace,
          title: 'Categories',
        ).getAppBar(context),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: _buildCategories(model),
            ),
          ),
        ),
      );
    });
  }
}
